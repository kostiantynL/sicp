(defn same [x] x)

(defn inc [x]
  (+ x 1))

; linear recursion

; (defn product [term a next b]
;   (if (> a b)
;     1
;     (* (term a)
;         (product term (next a) next b))))
      
; linear-iteration process

(defn product [term a next b]
  (defn iter [a result]
    (if (> a b)
      result
      (iter (next a) (* result (term a)))))
  (iter a 1))

(defn factorial [x]
  (product same 1 inc x))

(factorial 5)