(defn square [x]
  (* x x))

(defn even? [n]
  (= (rem n 2) 0))

(defn fast-expt [b n a]
  (cond
    (= n 0) 1
    (= n 1) (* a b)
    (even? n) (fast-expt (* b b) (/ n 2) a)
    :else (fast-expt b (- n 1) (* a b))))

(defn expt [b n]
  (fast-expt b n 1))

(defn expmod [base exp m]
  (rem (expt base exp) m))

(defn ferma-test [n a]
  (= (expmod a n n) a))

(defn ferma-prime? [n a]
  (cond 
    (= a n) true
    (ferma-test n a) (ferma-prime? n (+ a 1))
    :else false))

(defn prime? [n]
  (ferma-prime? n 0))

(prime? 561)
(prime? 1105)
(prime? 1729)
(prime? 2465)
(prime? 6601)
