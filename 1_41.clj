(defn double [f]
  #(f (f %)))

(defn inc [x]
  (+ x 1))

(((double (double double)) inc) 5)