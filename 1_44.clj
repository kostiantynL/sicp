(defn double [f]
  #(f (f %)))

(defn repeated [f count]
  (if (= count 1)
    #(f %)
    (repeated (double f) (- count 1))))

(defn smooth [f dx]
  (define average [x y z] (/ (+ x y z) 3))
  (fn [x] (average (f (- x dx))
                   (f x)
                   (f (+ x dx)))))

(defn repeated-smooth [f dx count]
  ((repeated (fn [nf] (smooth nf dx)) count) f))

