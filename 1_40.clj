(defn cubic [a b c]
  (fn [x] (+ (* a a a) (* a x x) (* b x) c)))
