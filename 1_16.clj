(defn square [x]
  (* x x))

(defn even? [n]
  (= (rem n 2) 0))

(defn fast-expt [b n a]
  (cond
    (= n 0) 1
    (= n 1) (* a b)
    (even? n) (fast-expt (* b b) (/ n 2) a)
    :else (fast-expt b (- n 1) (* a b))))

(defn expt [b n]
  (fast-expt b n 1))
