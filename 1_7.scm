(define (abs x)
  (cond ((< x 0)(- x))
    (else x)))

(define (sqrt-iter guess ear-guess x)
  (if (good-enough? guess ear-guess)
    guess
    (sqrt-iter (improve guess x) guess x)))
(define (improve guess x)
  (/ 
    (+ (/ x guess) guess)
     2))
(define (good-enough? guess ear-guess)
  (< (abs (- guess ear-guess)) 0.000001))
(define (sqrt x)
  (sqrt-iter 1.0 0.9 x))
(sqrt 36000000000000)
