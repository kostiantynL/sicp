(defn now [] (java.util.Date.))

(defn square [x]
  (* x x))

(defn expmod [base exp m]
  (cond
    (= exp 0) 1
    (even? exp)
      (rem (square (expmod base (/ exp 2) m))
        m)
    :else
      (rem (* base (expmod base (- exp 1) m))
        m)))

(defn fermat-test [n]
  (defn try-it [a]
    (= (expmod a n n) a))
(try-it (+ 1 (rand-int (- n 1)))))

(defn fast-prime? [n times]
  (cond 
    (= times 0) true
    (fermat-test n) (fast-prime? n (- times 1))
    :else false))

(defn timed-prime-test [n count]
  (start-prime-test n (.getTime (now)) count))

(defn start-prime-test [n start-time count]
  (if (fast-prime? n 3)
    (report-prime (- (.getTime (now)) start-time) n count)
    (s-f-p n count)))

(defn report-prime [elapsed-time n count]
  (println " *** ")
  (println elapsed-time)
  (println n)
  (newline)
  (s-f-p n (+ 1 count)))

(defn s-f-p [n count]
  (if (= count 3)
      (println "end")
      (timed-prime-test (+ 1 n) count)))

(defn search-for-primes [start]
  (s-f-p start 0))

(search-for-primes 1000)
(search-for-primes 10000)
(search-for-primes 100000)