(define (cube x) (* x x x))
(define (square x) (* x x))
(define (abs x)
  (cond ((< x 0)(- x))
    (else x)))

(define (cub-iter guess ear-guess x)
  (if (good-enough? guess ear-guess)
    guess
    (cub-iter (improve guess x) guess
      x)))
(define (improve guess x)
  (/ (+ 
    (/ x (square guess))
    (* 2 guess))
   3))
(define (good-enough? guess ear-guess)
  (< (abs (/ (- guess ear-guess) ear-guess)) 0.000001))
(define (cub x)
  (cub-iter 1.0 0.8 x))
(cub 8)
