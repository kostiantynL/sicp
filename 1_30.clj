(defn cube [x]
  (* x x x))

(defn inc [x]
  (+ x 1))

; initial variant
; (defn sum [term a next b]
;   (if (> a b)
;     0
;     (+ (term a)
;        (sum term (next a) next b))))

(defn sum [term a next b]
  (defn iter [a result]
    (if (> a b)
      result
      (iter (next a) (+ result (term a)))))
  (iter a 0))

(defn sum-of-cubes [a b]
  (sum cube a inc b))