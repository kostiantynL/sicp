(defn square [x] (* x x))
(defn sum-of-squares [x y]
  (+ (square x) (square y))
)
(defn abs [x]
  (cond
    (> x 0) x
    (= x 0) 0
    (< x 0) (- x)
    :else "bida"
  )
)
; 1.3
(defn bigSquares [x y z]
  (-
    (+ (square x) (square y) (square z))
    (square (min x y z))
  )
)
