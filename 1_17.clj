(defn even? [n]
  (= (rem n 2) 0))
(defn double [x]
  (+ x x))
(defn half [x]
  (/ x 2))
(defn * [a b]
  (cond
    (= b 0) 0
    (even? b) (double (* a (half b)))
    :else (+ a (* a (- b 1)))))