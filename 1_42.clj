(defn inc [x]
  (+ x 1))

(defn square [x] (* x x))

(defn compose [f1 f2]
  #(f1 (f2 %)))

((compose square inc) 6)