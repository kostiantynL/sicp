(defn abs [n]
  (if (< n 0) (- n) n))

(def tolerance 0.0001)

(defn fixed-point [f first-guess]
  (defn close-enough? [v1 v2]
    (< (abs (- v1 v2)) tolerance))
  (defn try-again [guess]
    (let [next (f guess)]
      (if (close-enough? guess next)
          next
          (try-again next))))
  (try-again first-guess ))

(fixed-point (fn [x] (+ 1 (/ 1 x))) 1.0)