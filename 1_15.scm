(define (cube x) (*  x x x))
(define (abs x)
  (cond ((< x 0)(- x))
    (else x)
  )
)

(define (p x) (* 1 x))

(define (sine angle counter)
  (if (not (> (abs angle) 0.1))
    counter
    (p (sine (/ angle 3.0) 
        (+ 1 counter)
    ))
  )
)

(sine 12.15 0)
