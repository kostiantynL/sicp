(defn square [x]
  (* x x))

(defn apply-trivial-check [k m r] 
  (if (and (not (= k 1)) 
           (not (= k (- m 1))) 
           (= r 1)) 
      0 
      r))

(defn rem-or-trivial [k m] 
  (apply-trivial-check k m (remainder (square k) m)))

(defn expmod [base exp m]
  (cond
    (= exp 0) 1
    (even? exp )
      (rem-or-trivial (square (expmod base (/ exp 2) m)) m)
    :else
      (rem-or-trivial (* base (expmod base (- exp 1) m)) m)))

(defn miller-rabin [n]
  (defn miller-rabin-iter [a t n]
    (defn try-it [a]
      (= (modified-expmod a (- n 1) n) 1))
    (if (= a n)
        (> t (/ n 2))
        (if (try-it a)
            (miller-rabin-iter (+ a 1) (+ t 1) n)
            (miller-rabin-iter (+ a 1) t n))))
  (miller-rabin-iteration 1 0 n))
