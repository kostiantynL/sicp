;Pascal triagnle (a+b)^n  --- summ of all elements in the row x---
(define (triangle a b row)
  (define (triangle-iter summ row)
    (if (= row 1)
      summ
      (triangle-iter (* summ (+ a b) ) (- row 1) )
    )
  )
  (triangle-iter (+ a b) row)
)

;row - element
(define (triangle row element)
  (cond ((or (= row element) (= element 1)) 1)
    ((> element row) NaN)
    ((or (< element 1) (< row 1)) NaN)
    (else(+ (triangle (- row 1) (- element 1))
      (triangle (- row 1) element )
    ))
  )
)

(triangle 4 3)