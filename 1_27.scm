(define (square x)
  (* x x))

(define (remainder x y)
  (cond
    ((< x y) x)
    (else (remainder (- x y) y))))

(define (even? n)
  (= (remainder n 2) 0))

(define (fast-expt b n a)
  (cond
    ((= n 0) 1)
    ((= n 1) (* a b))
    ((even? n) (fast-expt (* b b) (/ n 2) a))
    (else (fast-expt b (- n 1) (* a b)))))

(define (expt b n)
  (fast-expt b n 1))

(define (expmod base exp m)
  (remainder (expt base exp) m))

(define (ferma-test n a)
  (= (expmod a n n) a))

(define (ferma-prime? n a)
  (cond
    ((= a n) #t)
    ((ferma-test n a) (ferma-prime? n (+ a 1)))
    (else #f)))

(define (prime? n)
  (ferma-prime? n 0))

(prime? 561)
(prime? 1105)
(prime? 1729)
(prime? 2465)
(prime? 6601)
