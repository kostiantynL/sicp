; prime and helpers
(defn cube [x]
  (* x x x))

(defn inc [x]
  (+ x 1))

(defn same [x] x)

(defn square [x]
  (* x x))

(defn even? [n]
  (= (rem n 2) 0))

(defn fast-expt [b n a]
  (cond
    (= n 0) 1
    (= n 1) (* a b)
    (even? n) (fast-expt (* b b) (/ n 2) a)
    :else (fast-expt b (- n 1) (* a b))))

(defn expt [b n]
  (fast-expt b n 1))

(defn expmod [base exp m]
  (rem (expt base exp) m))

(defn ferma-test [n a]
  (= (expmod a n n) a))

(defn ferma-prime? [n a]
  (cond 
    (= a n) true
    (ferma-test n a) (ferma-prime? n (+ a 1))
    :else false))

(defn prime? [a b]
  (ferma-prime? a 0))

(defn gcd [a b]
  (if (= b 0)
      a
      (gcd b (rem a b))))

(defn gcd-prime? [a b]
  (= (gcd a b) 1))

; prime from 1.27

(defn filter-accomulate [filter combiner null-value term a next b]
  (cond (> a b) null-value
        (filter a b)(combiner (term a)
                            (filter-accomulate filter combiner null-value term (next a) next b))
        :else (filter-accomulate filter combiner null-value term (next a) inc b)))

(defn sum-prime-square-range [a b]
  (filter-accomulate prime? + 0 square a inc b))

(defn product-prime-gcd-range [a b]
  (filter-accomulate gcd-prime? * 1 same a inc b))

(product-prime-gcd-range 0 10)
(sum-prime-square-range 3 5)