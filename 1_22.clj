(defn now [] (java.util.Date.))
(defn square [x]
  (* x x))

(defn divides? [a b]
  (= (rem b a) 0))

(defn find-divisor [n test-divisor]
  (cond
    (> (square test-divisor) n) n
    (divides? test-divisor n) test-divisor
    :else (find-divisor n (+ test-divisor 1))))

(defn smallest-divisor [n]
  (find-divisor n 2))

(defn prime? [n]
  (= n (smallest-divisor n)))

(defn timed-prime-test [n count]
  (start-prime-test n (.getTime (now)) count))

(defn start-prime-test [n start-time count]
  (if (prime? n)
    (report-prime (- (.getTime (now)) start-time) n count)
    (s-f-p n count)))

(defn report-prime [elapsed-time n count]
  (println n)
  (println " *** ")
  (println elapsed-time)
  (s-f-p n (+ 1 count)))

(defn s-f-p [n count]

  (if (= count 3)
      (println "end")
      (timed-prime-test (+ 1 n) count)))

(defn search-for-primes [start]
  (s-f-p start 0))

(search-for-primes 10000)
(search-for-primes 100000)
(search-for-primes 1000000)
