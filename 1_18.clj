(defn even? [n]
  (= (rem n 2) 0))
(defn double [x]
  (+ x x))
(defn half [x]
  (/ x 2))
(defn multiply-iter [a b acc]
  (cond
    (= b 0) 0
    (= b 1) (+ a acc)
    (even? b) (multiply-iter (double a) (half b) acc)
    :else (multiply-iter a (- b 1) (+ acc a))))
(defn * [a b]
  (multiply-iter a b 0))
