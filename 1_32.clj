(defn cube [x]
  (* x x x))

(defn inc [x]
  (+ x 1))

(defn same [x] x)

(defn accomulate [combiner null-value term a next b]
  (if (> a b)
    null-value
    (combiner (term a)
        (accomulate combiner null-value term (next a) next b))))

; (defn accomulate [combiner null-value term a next b]
;   (defn iter [a result]
;     (if (> a b)
;       result
;       (iter (next a) (combiner result (term a)))))
;   (iter a null-value))


(defn factorial [x]
  (accomulate * 1 same 1 inc x))

(defn sum-of-cubes [a b]
  (accomulate + 1 cube a inc b))

(factorial 5)
(sum-of-cubes 2 3)